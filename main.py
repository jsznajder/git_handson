# na potrzeby git
import matplotlib.pyplot as plt

def poly_solve(a,b,c):
    delt=b*b-4*a*c
    x1=-b-(delt**0.5)/(2*a)
    x2=-b+(delt**0.5)/(2*a)
    # dodatkowy komentarz
    x_range=list(range(int(x1),int(x2)))
    y_range=[]
    for el in x_range:
        y_range.append(a*el*el+b*el+c)
    print('x_range: ',x_range)
    print('y_range: ',y_range)    
    return x1, x2, x_range, y_range


# komentarz dla gita
#     


def plot_from_file(fnm):
    l1,l2=read_data('dane.txt')
    plt.xlabel('Os x')
    plt.ylabel('Os y')    
    plt.plot(l1,l2,'-',label='test01');
    plt.title('Wykres funkcji')
    plt.savefig(fnm)
    plt.close
    